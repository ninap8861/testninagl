class-pool .
*"* class pool for class /SP2IT/CL1

*"* local type definitions
include /SP2IT/CL1====================ccdef.

*"* class /SP2IT/CL1 definition
*"* public declarations
  include /SP2IT/CL1====================cu.
*"* protected declarations
  include /SP2IT/CL1====================co.
*"* private declarations
  include /SP2IT/CL1====================ci.
endclass. "/SP2IT/CL1 definition

*"* macro definitions
include /SP2IT/CL1====================ccmac.
*"* local class implementation
include /SP2IT/CL1====================ccimp.

*"* test class
include /SP2IT/CL1====================ccau.

class /SP2IT/CL1 implementation.
*"* method's implementations
  include methods.
endclass. "/SP2IT/CL1 implementation
