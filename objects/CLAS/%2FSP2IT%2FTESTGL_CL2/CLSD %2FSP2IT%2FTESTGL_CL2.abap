class-pool .
*"* class pool for class /SP2IT/TESTGL_CL2

*"* local type definitions
include /SP2IT/TESTGL_CL2=============ccdef.

*"* class /SP2IT/TESTGL_CL2 definition
*"* public declarations
  include /SP2IT/TESTGL_CL2=============cu.
*"* protected declarations
  include /SP2IT/TESTGL_CL2=============co.
*"* private declarations
  include /SP2IT/TESTGL_CL2=============ci.
endclass. "/SP2IT/TESTGL_CL2 definition

*"* macro definitions
include /SP2IT/TESTGL_CL2=============ccmac.
*"* local class implementation
include /SP2IT/TESTGL_CL2=============ccimp.

*"* test class
include /SP2IT/TESTGL_CL2=============ccau.

class /SP2IT/TESTGL_CL2 implementation.
*"* method's implementations
  include methods.
endclass. "/SP2IT/TESTGL_CL2 implementation
